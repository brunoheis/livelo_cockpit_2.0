// import { request } from 'https';

'use strict';

var gulp = require('gulp'),
    less = require('gulp-less'),
    util = require('gulp-util'),
    usemin = require('gulp-usemin'),
    uglify = require('gulp-uglify'),
    htmlmin = require('gulp-htmlmin'),
    cleanCss = require('gulp-clean-css'),
    rev = require('gulp-rev'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    pump = require('pump'),
    replace = require('gulp-replace');

gulp.task('public-less', function() {
    gulp.src('./client/css/less/*.less')
        .pipe(less().on('error', util.log))
        .pipe(cleanCss())
        .pipe(rename({
            "suffix":'.min'
        }))
        .pipe(gulp.dest('client/css'));
});
gulp.task('public-watch',function() {
    gulp.watch('./client/css/less/*.less', ['public-less']);
});

gulp.task('private-less', function() {
    gulp.src('./client/ura/css/less/*.less')
        .pipe(less().on('error', util.log))
        .pipe(cleanCss())
        .pipe(rename({
            "suffix":'.min'
        }))
        .pipe(gulp.dest('client/ura/css'));
});
gulp.task('private-watch',function() {
    gulp.watch('./client/ura/css/less/*.less', ['private-less']);
});

gulp.task('default', ['public-watch', 'private-watch']);





gulp.task('controllers-minify', function() {
    gulp.src('./client/ura/js/controller/*.js')
        .pipe(less().on('error', util.log))
        .pipe(cleanCss())
        .pipe(rename({
            "suffix":'.min'
        }))
        .pipe(gulp.dest('client/ura/css'));
});
gulp.task('private-watch',function() {
    gulp.watch('./client/ura/css/less/*.less', ['private-less']);
});

gulp.task('default', ['public-watch', 'private-watch']);

gulp.task('replaceHml', function() {
    gulp.src('server/app/db/db-config.js')
        .pipe(replace('@port', '3306'))
        .pipe(replace('@host', '35.226.0.101'))
        .pipe(replace('@user', 'root'))
        .pipe(replace('@pass', 'i6zhgfFvMFs3D5sd'))
        .pipe(replace('@db', 'ipirangadbhom'))
        .pipe(gulp.dest('server/app/db/db-config-true/'));
});

gulp.task('replaceProd', function() {
    gulp.src('server/app/db/db-config.js')
        .pipe(replace('@port', '3306'))
        .pipe(replace('@host', '35.194.38.99'))
        .pipe(replace('@user', 'root'))
        .pipe(replace('@pass', '@HTzx_zhP4U9#89P'))
        .pipe(replace('@db', 'ipirangadbprod'))
        .pipe(gulp.dest('server/app/db/db-config-true/'));
});





// gulp.task('compress', function(cb) {

//     // <script src="/jquery/dist/jquery.min.js"></script>
// 	// <script src="/bootstrap/dist/js/bootstrap.min.js"></script>
//     pump([
//         gulp.src(['./node_modules/jquery/dist/jquery.min.js','./node_modules/bootstrap/dist/js/bootstrap.min.js']),
//         uglify(),
//         concat('jquery-bootstrap.min.js'),
//         gulp.dest('client/dependencies')
//     ]);


//     // <script src="/angular/angular.min.js"></script>
//     // <script src="/angular-animate/angular-animate.min.js"></script>
//     // <script src="/angular-ui-router/release/angular-ui-router.min.js"></script>
//     // <script src="/angular-translate/dist/angular-translate.min.js"></script>
//     // <script src="/angular-translate/dist/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js"></script>
//     // <script src="/angular-translate/dist/angular-translate-storage-local/angular-translate-storage-local.min.js"></script>
//     // <script src="/angular-sanitize/angular-sanitize.js"></script>
//     // <script src="/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js"></script>
//     pump([
//         gulp.src([
//             './node_modules/angular/angular.min.js',
//             './node_modules/angular-animate/angular-animate.min.js',
//             './node_modules/angular-ui-router/release/angular-ui-router.min.js',
//             './node_modules/angular-translate/dist/angular-translate.min.js',
//             './node_modules/angular-translate/dist/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
//             './node_modules/angular-translate/dist/angular-translate-storage-local/angular-translate-storage-local.min.js',
//             './node_modules/angular-sanitize/angular-sanitize.js',
//             './node_modules/angular-animate/angular-animate.min.js',
//             './node_modules/angular-animate/angular-animate.min.js',
//             './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js'
//         ]),
//         uglify(),
//         concat('angular-with-dependencies.min.js'),
//         gulp.dest('client/dependencies')
//     ]);


//     // <link rel="stylesheet" href="/bootstrap/dist/css/bootstrap.min.css">
//     // <link rel="stylesheet" href="/bootstrap/dist/css/bootstrap-theme.min.css">
//     pump([
//         gulp.src([
//             './node_modules/bootstrap/dist/css/bootstrap.min.css',
//             './node_modules/bootstrap/dist/css/bootstrap-theme.min.css'
//         ]),
//         cleanCss(),
//         concat('bootstrap.min.css'),
//         gulp.dest('client/dependencies')
//     ]);

//     // <script type="text/javascript" src="/cockpit/js/summernote-0.8.1.js"></script>
// 	// <script type="text/javascript" src="/angular-summernote/dist/angular-summernote.min.js"></script>
// 	// <script type="text/javascript" src="/codemirror/lib/codemirror.js"></script>
// 	// <script type="text/javascript" src="/codemirror/mode/javascript/javascript.js"></script>
// 	// <script type="text/javascript" src="/codemirror/mode/xml/xml.js"></script>
// 	// <script type="text/javascript" src="/angular-ui-codemirror/src/ui-codemirror.js"></script>
//     pump([
//         gulp.src([
//             './client/ura/js/summernote-0.8.1.js',
//             './node_modules/angular-summernote/dist/angular-summernote.min.js',
//             './node_modules/codemirror/lib/codemirror.js',
//             './node_modules/codemirror/mode/javascript/javascript.js',
//             './node_modules/codemirror/mode/xml/xml.js',
//             './node_modules/angular-ui-codemirror/src/ui-codemirror.js'
//         ]),
//         uglify(),
//         concat('other.min.js'),
//         gulp.dest('client/dependencies')
//     ]);


//     // <link rel="stylesheet" href="/summernote/dist/summernote.css">
//     // <link href="/codemirror/lib/codemirror.css" rel="stylesheet">
//     // <link href="/codemirror/addon/dialog/dialog.css" rel="stylesheet">
//     // <link href="/codemirror/addon/display/fullscreen.css" rel="stylesheet">
//     // <link href="/codemirror/addon/fold/foldgutter.min.css" rel="stylesheet">
//     // <link href="/codemirror/addon/hint/show-hint.min.css" rel="stylesheet">
//     pump([
//         gulp.src([
//             './node_modules/summernote/dist/summernote.css',
//             './node_modules/codemirror/lib/codemirror.css',
//             './node_modules/codemirror/addon/dialog/dialog.css',
//             './node_modules/codemirror/addon/display/fullscreen.css',
//             './node_modules/codemirror/addon/fold/foldgutter.min.css',
//             './node_modules/codemirror/addon/hint/show-hint.min.css'
//         ]),
//         cleanCss(),
//         concat('other.min.css'),
//         gulp.dest('client/dependencies')
//     ]);
// });
