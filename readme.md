# Livelo Cockpit 2.0

Este projeto contempla o Cockpit do chatbot da Livelo.

### Instalando dependências

Antes de tudo, certifique-se de ter o [Node.js](https://nodejs.org/) instalado.

Clone o projeto e instale as dependências:

npm init
npm install gulp

### Principais tasks do Grunt

O Gulpfile.js contém todas as tarefas automatizadas do projeto.
 - gulp public-less - para gerar a compilação do less na pasta client
 - gulp public-watch - faz o watch na compilação do less (usar apenas durante o desenvolvimento)
 - gulp private-less - para gerar a compilação do less na pasta ura
 - gulp private-watch - faz o watch na compilação do less (usar apenas durante o desenvolvimento)
 - gulp replaceHml - usada para mudar os dados do banco do dados para os de homologação
 - gulp replaceProd - usada para mudar os dados do banco do dados para os de produção

 ATENÇÃO: os comandos replaceHml e replaceProd geram a pasta ``db-config-true`` e um arquivo ``db-config`` dentro dela. Essa pasta e seu arquivo não devem ser commitados!

 Para mudar os apontamentos do banco, basta entrar no ``gulpfile.js ``, buscar por ``replaceHml`` ou ``replaceProd`` e mudar os dados referentes ao acesso do banco de homologação e produção, respectivamente.



Para mais informações, acesse https://steps.everis.com/confluence/display/EVIRTUALAT/3.+Configuring+Cockpit+Application e veja todas as informações sobre o Cockpit.