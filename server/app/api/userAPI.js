var userDAO = require('../DAO/userDAO');
var error = require('../error/error');

var api = {}




//<--------------------------------- SELECT ------------------------------------>


//SELECT ALL CHANNELS
api.selectAllUsers = function (req, res) {
	userDAO.select(function (err, users) {
		error.responseReturnXHR(res, err, users);
	});
};



//SELECT THE CHANNEL BY ID
api.selectUserById = function (req, res) {
	var id = req.params['id'];

	if (!id)
		return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"ID deve ser fornecido"}});

	userDAO.selectUserById(id, function (err, user){
		if (!err && !user)
			err = {
				"status": 204
			};
		error.responseReturnXHR(res, err, user);
	});
};


//LOGIN USER
api.findUser = function(req, res) {
	var user = req.body;

	if (!user.email)
		return res.status(400).end('{"message":"email deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!user.password)
		return res.status(400).end('{"message":"Password deve ser fornecido"}'); //TODO: internacionalizar erros

	userDAO.selectUserByPassword(user, function (err, user){
		if (!err && !user)
			err = {
				"status": 204
			};
		error.responseReturnXHR(res, err, user);
	});
};




//<--------------------------------- INSERT ------------------------------------>


//CREATE CHANNEL
api.createUser = function (req, res) {
	var user = req.body;

	if (!user.name)
		return res.status(400).end('{"message":"Nome do usuário deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!user.groupId && !user.statusAdmin && !user.ivr)
		return res.status(400).end('{"message":"Grupo do usuário deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!user.email)
		return res.status(400).end('{"message":"Email do usuário deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!user.password)
		return res.status(400).end('{"message":"Senha do usuário deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!user.confirmPassword)
		return res.status(400).end('{"message":"Senha do usuário deve ser confirmada"}'); //TODO: internacionalizar erros
	if (user.password != user.confirmPassword)
		return res.status(400).end('{"message":"Senhas não conferem"}'); //TODO: internacionalizar erros

	userDAO.selectUserByEmail(user.email, function(err, hasUser){
		if (err)
			return error.responseReturnXHR(res, err);
		
		if (hasUser)
			return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Já existe um usuário com o email fornecido"}}); //TODO: internacionalizar erros
		
		userDAO.insert(user, function (err){
			error.responseReturnXHR(res, err);
		});
	});
};


//<--------------------------------- UPDATE ------------------------------------>
api.updateUser = function (req, res) {
	var user = req.body;
	user.id = req.params['id'];

	if (!user.name)
		return res.status(400).end('{"message":"Nome do usuário deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!user.groupId && !user.statusAdmin && !user.ivr)
		return res.status(400).end('{"message":"Grupo do usuário deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!user.email)
		return res.status(400).end('{"message":"Email do usuário deve ser fornecido"}'); //TODO: internacionalizar erros
	if (user.password) {
		if (!user.confirmPassword)
			return res.status(400).end('{"message":"Senha do usuário deve ser confirmada"}'); //TODO: internacionalizar erros
		if (user.password != user.confirmPassword)
			return res.status(400).end('{"message":"Senhas não conferem"}'); //TODO: internacionalizar erros
	}

	userDAO.selectUserByEmail(user.email, function(err, userFound){
		if (err)
			return error.responseReturnXHR(res, err);

		if (userFound && userFound.id != user.id)
			return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Já existe um usuário com o email fornecido"}}); //TODO: internacionalizar erros
		
		userDAO.update(user, function (err) {
			error.responseReturnXHR(res, err);
		});
	});
};



//<--------------------------------- DELETE ------------------------------------>


//DELETE CHANNEL
api.deleteUser = function (req, res) {
	var id = req.params['id'];
	if (!id)
		return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"ID deve ser fornecido"}});

	userDAO.delete(id, function (err){
		error.responseReturnXHR(res, err);
	});
};







module.exports = api;