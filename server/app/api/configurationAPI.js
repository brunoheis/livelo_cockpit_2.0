var configurationDAO = require('../DAO/configurationDAO');
var error = require('../error/error');

var api = {}



//<--------------------------------- SELECT ------------------------------------>
//SELECT ALL ANSWER STATUSES
api.selectAllConfigurations = function (req, res) {
	configurationDAO.select(function (err, statuses) {
		error.responseReturnXHR(res, err, statuses);
	});
};


//<--------------------------------- INSERT ------------------------------------>
//CREATE CHANNEL
api.createConfiguration = function (req, res) {
	var configuration = req.body;

	if (!configuration.configKey)
		return res.status(400).end('{"message":"Chave deve ser fornecida"}'); //TODO: internacionalizar erros

	configurationDAO.selectConfigurationByConfigKey(configuration.configKey, function(err, hasConfig){
		if (err)
			return error.responseReturnXHR(res, err);
		
		if (hasConfig)
			return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Já existe um parâmetro com a chave fornecido"}}); //TODO: internacionalizar erros
		
		configurationDAO.insert(configuration, function (err){
			error.responseReturnXHR(res, err);
		});
	});
};


//<--------------------------------- UPDATE ------------------------------------>
api.updateConfiguration = function (req, res) {
	var configuration = req.body;
	var oldKey = req.params['configKey'];

	if (!configuration.configKey)
		return res.status(400).end('{"message":"Chave deve ser fornecida"}'); //TODO: internacionalizar erros

	configurationDAO.selectConfigurationByConfigKey(configuration.configKey, function(err, configFound){
		if (err)
			return error.responseReturnXHR(res, err);

		if (configFound){
			// return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Já existe um parâmetro com a chave fornecida"}}); //TODO: internacionalizar erros
			
			configurationDAO.update(oldKey, configuration, function (err) {
				error.responseReturnXHR(res, err);
			});
		}
	});
};



//<--------------------------------- DELETE ------------------------------------>


//DELETE CHANNEL
api.deleteConfiguration = function (req, res) {
	var oldKey = req.params['configKey'];
	if (!oldKey)
		return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Chave deve ser fornecida"}});

	configurationDAO.delete(oldKey, function (err){
		error.responseReturnXHR(res, err);
	});
};



module.exports = api;