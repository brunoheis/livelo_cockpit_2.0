var answerDirectoryDAO = require('../DAO/answer_directoryDAO');
var answerDAO = require('../DAO/answerDAO');
var error = require('../error/error');

var api = {}



//<--------------------------------- SELECT ------------------------------------>
//SELECT THE ANSWER_DIRECTORY BY ID
api.selectRootDirectories = function (req, res) {
	var projectId = req.params['projectId'];
	answerDirectoryDAO.selectAnswerDirectoryByParentId(projectId, function (err, answerDirectories){
		if (!err && (!answerDirectories || !answerDirectories.length))
			err = {
				"status": 204
			};
		error.responseReturnXHR(res, err, answerDirectories);
	});
};

api.selectDirectoryContent = function (req, res) {
	var parentId = req.params['id'];
	var projectId = req.params['projectId'];

	answerDirectoryDAO.selectAnswerDirectoryByParentId(projectId, parentId, function (err, answerDirectories){
		if (err)
			return error.responseReturnXHR(res, err);
		
		answerDAO.selectAnswerByDirectory(parentId, function(err, answers) {
			error.responseReturnXHR(res, err, {
				'answers':answers,
				'directories':answerDirectories
			});
		});
	});
};


//<--------------------------------- INSERT ----------------------------------->


//CREATE ANSWER_DIRECTORY
api.createAnswerDirectory = function (req, res) {
	var answerdirectory = req.body;
	var projectId = req.params['projectId'];
	answerdirectory.projectId = projectId;

	if (!answerdirectory.name)
		return res.status(400).end('{"message":"Nome da pasta deve ser fornecido"}'); //TODO: internacionalizar erros
	

	answerDirectoryDAO.selectCountByName(projectId, answerdirectory.name, false, function(err, count){
		if (err)
			return error.responseReturnXHR(res, err);
		
		if (count > 0)
			return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Já existe uma pasta com o nome fornecido"}}); //TODO: internacionalizar erros
		
		answerDirectoryDAO.insert(answerdirectory, function (err){
			error.responseReturnXHR(res, err);
		});
	});
};


//<--------------------------------- UPDATE ------------------------------------>


//UPDATE NAME OF ANSWER_DIRECTORY 
api.updateAnswerDirectory = function (req, res) {
	var answerdirectory = req.body;
	answerdirectory.id = req.params['id'];
	answerdirectory.projectId = req.params['projectId'];

	if (!answerdirectory.name)
		return res.status(400).end('{"message":"Nome da pasta deve ser fornecido"}'); //TODO: internacionalizar erros

	answerDirectoryDAO.selectCountByName(answerdirectory.projectId, answerdirectory.name, answerdirectory.id, function(err, count){
		if (err)
			return error.responseReturnXHR(res, err);

		if (count > 0)
			return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Já existe uma pasta com o nome fornecido"}}); //TODO: internacionalizar erros
		
		answerDirectoryDAO.update(answerdirectory, function (err) {
			error.responseReturnXHR(res, err);
		});
	});
};

//<--------------------------------- DELETE ------------------------------------>


//DELETE ANSWER_DIRECTORY
api.deleteAnswerDirectory = function (req, res) {
	var id = req.params['id'];
	if (!id)
		return error.responseReturnXHR(res, {"status": 400, "returnObj":{"message":"ID deve ser fornecido"}});

	answerDirectoryDAO.delete(id, function (err){
		error.responseReturnXHR(res, err);
	});

};



module.exports = api;
