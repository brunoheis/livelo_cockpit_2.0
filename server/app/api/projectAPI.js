var projectDAO = require('../DAO/projectDAO');
var error = require('../error/error');

var api = {}

//<--------------------------------- SELECT ------------------------------------>


//SELECT ALL PROJECT
api.selectUserProjects = function (req, res) {
	if (req.user.statusAdmin == '1') {
		projectDAO.select(function (err, projects) {
			// for(proj of projects) {
			// 	if (proj.image) {
			// 		var image = new Buffer(proj.image, 'binary').toString('base64');
			// 		proj.image = proj.contentType+image;
			// 	}
			// }
			// err

			error.responseReturnXHR(res, err, projects);
		});
	} else {
		projectDAO.selectProjectsByUser(req.user.id, function (err, projects) {
			// for(proj of projects) {
			// 	if (proj.image) {
			// 		var image = new Buffer(proj.image, 'binary').toString('base64');
			// 		proj.image = proj.contentType+image;
			// 	}
			// }
			// err
			error.responseReturnXHR(res, err, projects);
		});
	}
};

api.selectProjectImage = function(req, res) {
	console.log(req.params['id']);
	projectDAO.selectImageById(req.params['id'], function(err, proj) {
		if (err)
			return res.status(500).end();

		if (!proj)
			return res.status(204).end();
		
		res.writeHead(200, {'Content-Type': proj.contentType });
		res.end(proj.image, 'binary');
	});
};

//SELECT THE PROJECT BY USER

api.selectProjectsByUserId = function (req,res){
	var userid = req.params['userid'];

	if (!userid)
		return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"userId deve ser fornecido"}});

	projectDAO.selectProjectsByUser(userid,function (err,channel){
		if (!err && !channel)
			err = {
				
				"status": 204
			};
		error.responseReturnXHR(res, err, channel);
	});
}


//SELECT THE PROJECT BY ID
api.selectProjectById = function (req, res) {
	var id = req.params['id'];

	if (!id)
		return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"ID deve ser fornecido"}});

	projectDAO.selectProjectById(id, function (err,channel){
		if (!err && !channel)
			err = {
				"status": 204
			};
		error.responseReturnXHR(res, err, channel);
	});
};


//<--------------------------------- INSERT ----------------------------------->


//CREATE PROJECT
api.createProject = function (req, res) {
	var project = req.body;

	if (!project.name)
		return res.status(400).end('{"message":"Nome do projeto deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!project.workspaceId)
		return res.status(400).end('{"message":"workspaceId do projeto deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!project.workspaceUsername)
		return res.status(400).end('{"message":"workspaceUsername do projeto deve ser fornecida"}'); //TODO: internacionalizar erros
	if (!project.workspacePassword)
		return res.status(400).end('{"message":"workspacePassword do projeto deve ser fornecida"}'); //TODO: internacionalizar erros

	projectDAO.selectCountByName(project.name, false, function(err, count){
		if (err)
			return error.responseReturnXHR(res, err);
		
		if (count > 0)
			return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Já existe um projeto com o nome fornecido"}}); //TODO: internacionalizar erros
		
		if (project.image)
			project.image = Buffer.from(project.image, "base64");

		
		projectDAO.insert(project, req.user.name, function (err){
			error.responseReturnXHR(res, err);
		});
	});
};




//<--------------------------------- UPDATE ------------------------------------>


//UPDATE NAME OF PROJECT
api.updateProject = function (req, res) {
	var project = req.body;
	project.id = req.params['id'];

	if (!project.name)
		return res.status(400).end('{"message":"Nome do projeto deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!project.workspaceId)
		return res.status(400).end('{"message":"workspaceId do projeto deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!project.workspaceUsername)
		return res.status(400).end('{"message":"workspaceUsername do projeto deve ser fornecida"}'); //TODO: internacionalizar erros
	if (!project.workspacePassword)
		return res.status(400).end('{"message":"workspacePassword do projeto deve ser fornecida"}'); //TODO: internacionalizar erros

	projectDAO.selectCountByName(project.name, project.id, function(err, count){
		if (err)
			return error.responseReturnXHR(res, err);

		if (count > 0)
			return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Já existe um projeto com o nome fornecido"}}); //TODO: internacionalizar erros
		
		if (project.image)
			project.image = Buffer.from(project.image, "base64");
			
		projectDAO.update(project, req.user.name, function (err) {
			error.responseReturnXHR(res, err);
		});
	});
};

//<--------------------------------- DELETE ------------------------------------>


//DELETE CHANNEL
api.deleteProject = function (req, res) {
	var id = req.params['id'];
	if (!id)
		return error.responseReturnXHR(res, {"status": 400, "returnObj":{"message":"ID deve ser fornecido"}});

	projectDAO.delete(id, function (err){
		error.responseReturnXHR(res, err);
	});

};



module.exports = api;
