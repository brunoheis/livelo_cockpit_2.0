var channelDAO = require('../DAO/channelDAO');
var error = require('../error/error');

var api = {}

//<--------------------------------- SELECT ------------------------------------>


//SELECT ALL CHANNELS
api.selectAllChannels = function (req, res) {
	var projectId = req.params["projectId"];
	channelDAO.select(projectId, function (err, channels) {
		error.responseReturnXHR(res, err, channels);
	});
};



//SELECT THE CHANNEL BY ID
api.selectChannelById = function (req, res) {
	var id = req.params['id'];
	var projectId = req.params["projectId"];

	if (!id)
		return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"ID deve ser fornecido"}});

	channelDAO.selectChannelById(projectId, id, function (err,channel){
		if (!err && !channel)
			err = {
				"status": 204
			};
		error.responseReturnXHR(res, err, channel);
	});
};




//<--------------------------------- INSERT ------------------------------------>


//CREATE CHANNEL
api.createChannel = function (req, res) {
	var channel = req.body;
	var projectId = req.params["projectId"];
	channel.projectId = projectId;

	if (!channel.name)
		return res.status(400).end('{"message":"Nome do canal deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!channel.projectId)
		return res.status(400).end('{"message":"Projeto do canal deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!channel.classificationId)
		return res.status(400).end('{"message":"Classificação do canal deve ser fornecida"}'); //TODO: internacionalizar erros

	channelDAO.selectCountByName(channel.name, false, function(err, count){
		if (err)
			return error.responseReturnXHR(res, err);
		
		if (count > 0)
			return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Já existe um canal com o nome fornecido"}}); //TODO: internacionalizar erros
		
		channelDAO.insert(channel, req.user.name, function (err){
			error.responseReturnXHR(res, err);
		});
	});
};


//<--------------------------------- UPDATE ------------------------------------>
api.updateChannel = function (req, res) {
	var channel = req.body;
	channel.id = req.params['id'];
	var projectId = req.params["projectId"];
	channel.projectId = projectId;

	if (!channel.name)
		return res.status(400).end('{"message":"Nome do canal deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!channel.projectId)
		return res.status(400).end('{"message":"Projeto do canal deve ser fornecido"}'); //TODO: internacionalizar erros
	if (!channel.classificationId)
		return res.status(400).end('{"message":"Classificação do canal deve ser fornecida"}'); //TODO: internacionalizar erros

	channelDAO.selectCountByName(channel.name, channel.id, function(err, count){
		if (err)
			return error.responseReturnXHR(res, err);

		if (count > 0)
			return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"Já existe um canal com o nome fornecido"}}); //TODO: internacionalizar erros
		
		channelDAO.update(channel, req.user.name, function (err) {
			error.responseReturnXHR(res, err);
		});
	});
};



//<--------------------------------- DELETE ------------------------------------>


//DELETE CHANNEL
api.deleteChannel = function (req, res) {
	var id = req.params['id'];
	var projectId = req.params["projectId"];
	if (!id)
		return error.responseReturnXHR(res, {"status":400, "returnObj":{"message":"ID deve ser fornecido"}});

	channelDAO.delete(projectId, id, function (err){
		error.responseReturnXHR(res, err);
	});
};





module.exports = api;