var sqlUtil = require('../sql-util');

var api = {}


//<--------------------------------- SELECT ------------------------------------>
//SELECT ALL CHANNEL CLASSIFICATION IN DATABASE.
api.select = function (callback) {
	
    var sqlMapSelect = {};
    sqlMapSelect.table ="channel_classification"; 

    sqlUtil.executeQuery(sqlMapSelect, function(err, rows) {
		if (err)
			return callback(err);

        callback(null, rows);
    });
        	
};



//SELECT CHANNEL CLASSIFICATION IN DATABASE BY ID
api.selectChannelClassifById = function (id, callback) {
    var sqlMapSelect = {
        "table": "channel_classification",
        "where": {
            "id" : id
        }
    };

    sqlUtil.executeQuery(sqlMapSelect, function(err, rows) {
		if (err)
			return callback(err);
        
        callback(null, rows.length > 0 ? rows[0] : null);
    });
        	
};


module.exports = api;