var sqlUtil = require('../sql-util');
var api = {}

//<--------------------------------- SELECT ------------------------------------>

//SELECT PROJECTS IN DATABASE BY USER PARM sqlMapSelect
api.selectProjectsByUser = function(userId, callback) {
    var sqlMap = {
        "table": "project_user pu, project p",
        "fields":["p.id, p.name, p.locale"],
        "where":[
            {"pu.userId":userId},
            "pu.projectId = p.id"
        ]
    };
    sqlUtil.executeQuery(sqlMap, function(err, rows) {
        callback(err, rows);
    });
};

//SELECT ALL PROJECT
api.select = function (callback) {
    var sqlMapSelect = {"fields":[
        "id","name","workspaceId","workspaceUsername","workspacePassword","description","locale" 
    ]};
    sqlMapSelect.table ="project"; 

    sqlUtil.executeQuery(sqlMapSelect, function(err, rows) {
		if (err)
			return callback(err);

        callback(null, rows);
    });
        	
};

//SELECT ALL PROJECT
api.selectSimplified = function (callback) {
    var sqlMapSelect = {"fields":[
        "id","name" 
    ]};
    sqlMapSelect.table ="project"; 

    sqlUtil.executeQuery(sqlMapSelect, function(err, rows) {
		if (err)
			return callback(err);

        callback(null, rows);
    });
        	
};

api.selectByUser = function(userId, callback) {
    var sqlMap = {
        "table": "project_user pu, project p",
        "fields":["p.id, p.name, p.workspaceId, p.workspaceUsername, p.workspacePassword, p.description, p.contentType, p.locale"],
        "where":[
            {"pu.userId":userId},
            "pu.projectId = p.id"
        ]
    };

    sqlUtil.executeQuery(sqlMapSelect, function(err, rows) {
		if (err)
			return callback(err);

        callback(null, rows);
    });
};

api.selectImageById = function(id, callback) {
    var sqlMapSelect = {
        "table": "project",
        "fields": ["image", "contentType"],
        "where": {
            "id" : id
        }
    };

    sqlUtil.executeQuery(sqlMapSelect, function(err, rows) {
		if (err)
			return callback(err);

        callback(null, rows && rows.length ? rows[0] : null);
    });
};

//SELECT PROJECT IN DATABASE BY ID
api.selectProjectById = function (id, callback) {
    var sqlMapSelect = {
        "table": "project",
        "where": {
            "id" : id
        }
        
    };

    sqlUtil.executeQuery(sqlMapSelect, function(err, rows) {
		if (err)
			return callback(err);
        
        callback(null, rows.length > 0 ? rows[0] : null);
    });
        	
};


api.selectCountByName = function (name, id, callback) {
    var sqlMapSelect = {
        "table": "project",
        "where": [
            {"name": name}
        ],
        "fields": "count(1) as qt"
    };
    if (id)
        sqlMapSelect.where.push(" id <> " + id + " ");

    sqlUtil.executeQuery(sqlMapSelect, function(err, rows) {
        if (err)
            return callback(err);
        callback(null, rows[0].qt);
    });
}


//<------------------------------- INSERT ----------------------------------->
//VALIDADE IF EXISTS CHANNEL IN DATABASE AND INSERT BY PARM sqlMapInsert
api.insert = function (project, userName, callback) {
    var sqlMapInsert = {
        "table": "project",
        "fields":{
            "name": project.name,
            "workspaceId": project.workspaceId,
            "workspaceUsername": project.workspaceUsername,
            "workspacePassword": project.workspacePassword,
            "description":project.description,
            "image": project.image,
            "contentType": project.contentType,
            "locale": project.locale,
            "createdBy": userName,
            "createDate": new Date()
            },
        "type":"insert"
        };


    sqlUtil.executeQuery(sqlMapInsert, function(err, rows) {
        if (err)
            return callback(err);
        return callback();
    });
};


//<--------------------------------- UPDATE ------------------------------------>

api.update = function(project, userName, callback) {
    var sqlMapUpdate = {
        'table':'project',
        "fields":{
            "name": project.name,
            "workspaceId": project.workspaceId,
            "workspaceUsername": project.workspaceUsername,
            "workspacePassword": project.workspacePassword,
            "description":project.description,
            "locale": project.locale,
            "updatedBy": userName,
            "updateDate": new Date()
          },
        'where': {
            'id':project.id
        },
        'type':'update'
    };

    if (project.image) {
        sqlMapUpdate.fields['image'] = project.image;
        sqlMapUpdate.fields['contentType'] = project.contentType;
    }

    sqlUtil.executeQuery(sqlMapUpdate, function(err, rows) {
		if (err)
			return callback(err);
        callback();
    });
        
};





//<--------------------------------- DELETE ------------------------------------>



//DELETE CHANNEL BY PARM sqlMapDelete
api.delete = function (id, callback) {
	
    var sqlMapDelete = {
        table: "project",
        where: {
            'id':id
        },
        type:"delete"
    };

    sqlUtil.executeQuery(sqlMapDelete, function(err) {
       
		if (err)
			return callback(err);

        callback(null);
        
    });
        	
};






module.exports = api;

