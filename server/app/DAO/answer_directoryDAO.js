var sqlUtil = require('../sql-util');

var api = {}



//<--------------------------------- SELECT ------------------------------------>
//SELECT ANSWER_DIRECTORY IN DATABASE BY PARENTID
api.selectAnswerDirectoryByParentId = function (projectId, parentId, callback) {
    if (typeof(parentId) == 'function') {
        callback = parentId;
        parentId = null;
    }
    var sqlMapSelect = {
        "table": "answer_directory",
        "where": [
            {"projectId":projectId}
        ]
    };
    if (parentId)
        sqlMapSelect.where.push({"parentId":parentId});
    else
        sqlMapSelect.where.push("parentId is null");

    sqlUtil.executeQuery(sqlMapSelect, function(err, rows) {
		if (err)
			return callback(err);
        
        callback(null, rows);
    });
        	
};


api.selectCountByName = function (projectId, name, id, callback) {
    var sqlMapSelect = {
        "table": "answer_directory",
        "where": [
            {"name": name},
            {"projectId":projectId}
        ],
        "fields": "count(1) as qt"
    };
    if (id)
        sqlMapSelect.where.push(" id <> " + id + " ");

    sqlUtil.executeQuery(sqlMapSelect, function(err, rows) {
        if (err)
            return callback(err);
        callback(null, rows[0].qt);
    });
}


//<------------------------------- INSERT ----------------------------------->
//VALIDADE IF EXISTS ANSWER_DIRECTORY IN DATABASE AND INSERT BY PARM sqlMapInsert
api.insert = function (answerdirectory, callback) {
    var sqlMapInsert = {
        "table": "answer_directory",
        "fields":{
                "name": answerdirectory.name,
                "parentId": answerdirectory.parentId,
                "projectId": answerdirectory.projectId
            },
        "type":"insert"
        };


    sqlUtil.executeQuery(sqlMapInsert, function(err, rows) {
        if (err)
            return callback(err);
        return callback();
    });
};


//<--------------------------------- UPDATE ------------------------------------>

api.update = function(answerdirectory, callback) {
    var sqlMapUpdate = {
        'table':'answer_directory',
        "fields":{
            "name": answerdirectory.name,
            "projectId": answerdirectory.projectId
          },
        'where': {
            'id':answerdirectory.id
        },
        'type':'update'
    };


    sqlUtil.executeQuery(sqlMapUpdate, function(err, rows) {
		if (err)
			return callback(err);
        callback();
    });
        
};





//<--------------------------------- DELETE ------------------------------------>



//DELETE ANSWER_DIRECTORY BY PARM sqlMapDelete
api.delete = function (id, callback) {
	
    var sqlMapDelete = {
        table: "answer_directory",
        where: {
            'id':id
        },
        type:"delete"
    };

    sqlUtil.executeQuery(sqlMapDelete, function(err) {
       
		if (err)
			return callback(err);

        callback(null);
        
    });
        	
};






module.exports = api;

