var api = require('../api/sessionAPI');
var permissions = require('../../config/permissions');

module.exports = function(app) {

    app.post('/cockpit/v1/projects/:projectId/sessions', api.findSessions);

};
