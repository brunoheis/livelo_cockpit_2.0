var https = require('https');

module.exports = function(app) {

    require('./permissions_routes')(app);
    require('./permissionsgroup_routes')(app);
    require('./groups_routes')(app);
    require('./channels_routes')(app);
    require('./projects_routes')(app);
    require('./user_routes')(app);
    require('./projectsuser_routes')(app);
    require('./channelclassification_routes')(app);
    require('./answerStatus_routes')(app);
    require('./configuration_routes')(app);
    require('./answerdirectory_routes')(app);
    require('./answer_routes')(app);
    require('./session_routes')(app);
    require('./sample_routes')(app);
    require('./ivr_routes')(app);

    app.get('/cockpit/v1/user', function(req, res) {
        if (req.user)
            res.status(200).jsonp(req.user);
        else
            res.status(204).end();
    });
    
    app.get('/ura/vivo-info/:phoneNumber', function(req, res) {
    	https.request({
    		host: 'poccall.azurewebsites.net',
    		path: '/accounts/'+req.params['phoneNumber']+'/balances',
    		method: 'GET',
    		headers:{
    			'Authorization': 'Basic ZXZlcmlzOmV2ZXJpczEyMzQ1Ng=='
    		}
    	}, function(resp) {
    		let data = '';
    		resp.on('data', (chunk) => {
    			data += chunk;
    		});
    		resp.on('end', () => {
    			console.log(JSON.parse(data));
    			res.end(data);
    		});
    	}).end();
    });
};

