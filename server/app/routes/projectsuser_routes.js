var apiProjectUser = require('../api/projectsuserAPI');

module.exports = function(app) {

    app.get('/cockpit/v1/projectsuser', apiProjectUser.selectAllProjectUser);
    app.get('/cockpit/v1/projectsuser/:id', apiProjectUser.selectProjectUserById);
    app.post('/cockpit/v1/projectsuser', apiProjectUser.createProjectUser);
    app.put('/cockpit/v1/projectsuser/:projectIdKey/:userIdKey', apiProjectUser.updateProjectUser);
    app.delete('/cockpit/v1/projectsuser/:projectIdKey/:userIdKey', apiProjectUser.deleteProjectUser);

};

