var apiPermission = require('../api/permissionAPI');

module.exports = function(app) {

    app.get('/cockpit/v1/permissions', apiPermission.selectAllPermissions);
    app.get('/cockpit/v1/permissions/:id', apiPermission.selectPermissionById);

};