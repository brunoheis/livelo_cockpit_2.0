var apiPermissionGroup = require('../api/permission_groupAPI');

module.exports = function(app) {

    app.get('/cockpit/v1/permissionsgroup/:id', apiPermissionGroup.selectPermissionGroupByGroupId);
    app.put('/cockpit/v1/permissionsgroup', apiPermissionGroup.updatePermissionGroup);
    
};