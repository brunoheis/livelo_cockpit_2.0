var apiConfiguration = require('../api/configurationAPI');
var permissions = require('../../config/permissions');

module.exports = function(app) {

    app.get('/cockpit/v1/configurations',permissions.any('SEARCH_CONFIG'), apiConfiguration.selectAllConfigurations);
    app.post('/cockpit/v1/configurations',permissions.any('CREATE_CONFIG'), apiConfiguration.createConfiguration);
    app.put('/cockpit/v1/configurations/:configKey',permissions.any('ALTER_CONFIG'), apiConfiguration.updateConfiguration);
    app.delete('/cockpit/v1/configurations/:configKey',permissions.any('REMOVE_CONFIG'), apiConfiguration.deleteConfiguration);

};
