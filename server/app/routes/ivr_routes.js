var ivrAPI = require('../api/ivrAPI');

module.exports = function(app) {
    app.get('/ura/v1/ivr/human-redirect/:phoneNumber/open', ivrAPI.humanRedirectOpen);
    app.get('/ura/v1/ivr/human-redirect/:phoneNumber/close', ivrAPI.humanRedirectClose);
    app.get('/ura/v1/ivr/has-redirection', ivrAPI.hasRedirection);
    app.get('/ura/v1/ivr/projects', ivrAPI.projects);
};