var apiChannelClassification = require('../api/channelClassificationAPI');

module.exports = function(app) {

    app.get('/cockpit/v1/channelClassifications', apiChannelClassification.selectAllChannelClassification);
    app.get('/cockpit/v1/channelClassifications/:id', apiChannelClassification.selectChannelClassificationById);


};