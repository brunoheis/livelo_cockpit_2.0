var user_interactionAPI = require('../api/user_interactionAPI')

module.exports = function(app) {

    app.post('/cockpit/v1/projects/:projectId/userinteraction/search', user_interactionAPI.findUserInteraction);

}