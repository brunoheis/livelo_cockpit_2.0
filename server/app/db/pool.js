//var config = require('./db-config-true/db-config')
var config = require('./db-config');
var mysql = require("mysql");

// First you need to create a connection to the db
var pool = mysql.createPool({
	port:config.prod.port,
    host:config.prod.host,
    user:config.prod.user,
    password :config.prod.pass,
    database:config.prod.database
});

global.connectionPool = pool;

var poold1 = mysql.createPool({
	port:config.d1.port,
    host:config.d1.host,
    user:config.d1.user,
    password :config.d1.pass,
    database:config.d1.database
});

global.connectionPoolD1 = poold1;