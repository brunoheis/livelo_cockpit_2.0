var app = angular.module("loginApp", ['pascalprecht.translate']);


app.controller('loginCtrl', function($scope,$http) {
	$scope.wrongEmail = false;
	$scope.wrongPassword = false;
	$scope.errorText = '';
	
	$scope.login = function(e){
		$scope.wrongEmail = false;
		$scope.wrongPassword = false;
		$scope.wrongEmail = !$scope.email || $scope.email.indexOf('@')<0;
		$scope.wrongPassword = !$scope.password;

		if ($scope.wrongPassword && $scope.wrongEmail)
			$scope.errorText = 'LOGIN-FIELDS-ERROR';
		else if ($scope.wrongEmail)
			$scope.errorText = 'LOGIN-EMAIL-ERROR';
		else if ($scope.wrongPassword)
			$scope.errorText = 'LOGIN-PASSWORD-ERROR';
		else
			$scope.doLogin();

		e.preventDefault();
	};

	
	$scope.doLogin = function() {
		var data = {
			"email":$scope.email,
			"password":$scope.password
		};
		$http.post('/login',data).then(
			//success
			function(res) {
				window.location.href = res.data == 'IVR' ? '/ura/' : '/cockpit/';
			},
			//error
			function (res) {
				$scope.errorText = res.data;
				$scope.wrongEmail = $scope.wrongPassword = true;
			}
		);
	}

});



app.config(function($translateProvider) {	
	$translateProvider.useStaticFilesLoader({
		prefix: '/lang/',
		suffix: '.json'
	});

	$translateProvider.useSanitizeValueStrategy('escapeParameters');
	$translateProvider.preferredLanguage('pt');
});



