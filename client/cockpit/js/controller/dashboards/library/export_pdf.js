$(function () {

    $('#BUTTON_EXPORT_PDF').click(function () {
        var doc = new jsPDF('l', 'pt', 'a4');
        doc.addHTML($('#content'), 30,30, {
        pagesplit:true,
        background: '#e6eeef',
        image:{ type: 'jpeg', quality: 1},
        html2canvas:{ dpi: 400 } 
    }, function() {
        doc.save('export.pdf');
        });
    });

});