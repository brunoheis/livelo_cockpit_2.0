
// ------------------------- GERAL -------------------------- //


//QLIK SERVER PARMS
var parmsQlikServer = {
    host : "qlik-everis.everisva.com",
    prefix :  "/",
    port : "443",
    isSecure : true
};

//QLIK APP IN QLIK SERVER
var parmsQlikApp = {
    appId: "536d35a5-da6b-43dd-bcad-203f71018d0c"
};


//USER DATA
var userData = {
    type : window.parent.qlikIsAdminUser,
    qlikName : window.parent.userDataQlik.userQlik
};


//ID RULES TO ADMIN
var ruleAdmin = [1];


//LANGUAGE ACTUAL DASHBOARDS
var dashboardsLanguageName = window.parent.qlikLanguageActual;




// --------------------------- SESSION ---------------------------- //

//FIELD AND VALUE LIMIT TO PUT OPACITY SCREEN
var parmsFieldControlScreenLoadSession = [
    {nameField:'Data Sessão', valueLimit: 31}
];


//FIELD SELECTED IN LOAD OF SESSION
var parmsFieldSelectedLoadSession = [
    {nameField:'Projeto', value:window.parent.qlikProjectActual}
];

// -------------------------- QUESTION ---------------------------- //


//FIELD AND VALUE LIMIT TO PUT OPACITY SCREEN
var parmsFieldControlScreenLoadQuestion = [
    {nameField:'Data User', valueLimit: 31}
];

//FIELD SELECTED IN LOAD OF SESSION
var parmsFieldSelectedLoadQuestion = [
    {nameField:'Projeto', value:window.parent.qlikProjectActual}
];



// ------------------------- SATISFACTION -------------------------- //

//FIELD AND VALUE LIMIT TO PUT OPACITY SCREEN SATISFACTION
var parmsFieldControlScreenLoadSatisfaction = [
    {nameField:'Data Satisfação', valueLimit: 31}
];

//FIELD SELECTED IN LOAD OF SESSION
var parmsFieldSelectedLoadSatisfaction = [
    {nameField:'Projeto', value:window.parent.qlikProjectActual}
];












