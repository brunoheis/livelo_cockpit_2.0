var app = angular.module("cockpitApp");

app.controller('answerCtrl',function($scope, $http, $location,$filter,$interval, leftBar,confirmModal,messages,project, loading) {
    leftBar.show('answer');

    // Answer Modal:
	var uri = '/cockpit/v1/answers/' + project.get().locale + '/' + project.get().id +'/export';
    var summerOptions = [
		['fontname',['fontname']],
		['fontsize',['fontsize']],
		['style',['bold','italic','underline']],
		['color',['color']],
		['link',['link']],
		['paragraph',['paragraph']]
	];
    $scope.summerOptions = {
		height: 150,
		toolbar: summerOptions,
		dialogsInBody: true
    };
	
	$scope.parentDir = null;
	$scope.channelName = 'ALL';

	$scope.answer = {byChannel:{}};
	$scope.answerError = {};
	$scope.status = [];
	$scope.faqsFilter = [];
	$scope.codesFilter = [];
	$scope.textsFilter = [];

	$scope.abrir = function() {
		$('#answerModal').modal("show");
	}

	$scope.isChannelName = function(n) {
		return n == $scope.channelName;
	}

	$scope.setChannelName = function(n) {
		$scope.channelName = n;
	}

	$scope.replaceChannelName = function(cn) {
		return cn.replace(/ /g,'-');
	}

	$scope.enableContent = function(cn) {
		if (!cn)cn = $scope.channelName;
		if ($scope.answer.byChannel[cn])
			$scope.answer.byChannel[cn].enabled = true;
		else{ 
			$scope.answer.byChannel[cn] = {
				"enabled":true,
				"answerType":"HTML",
				"techTextType":"javascript"
			};
		$scope.answer.byChannel[cn].enabled = true;
		}
		$('.channel-selector').find("div[id*='channel']").each(function(e){
			console.log("teste" + this);
			this.classList.remove('active-channel');
		});
		for(channel in $scope.answer.byChannel){
			if($scope.answer.byChannel[channel].active === 1)
				$('#channel-'+ channel.replace(" ", "-")).addClass('active-channel');
			else{
				if($scope.answer.byChannel[channel].enabled === true)
					$('#channel-'+ channel.replace(" ", "-")).addClass('active-channel');
				else
					$('#channel-'+ channel.replace(" ", "-")).removeClass('active-channel');
			}
		}
		
	}

	$scope.disableContent = function() {
		var a = $scope.answer.byChannel[$scope.channelName];
		if (a.id) {
			$scope.answer.byChannel[$scope.channelName].enabled = false;
		} else {
			$scope.answer.byChannel[$scope.channelName].enabled = false;
			delete $scope.answer.byChannel[$scope.channelName];
		}
		$('#channel-'+ $scope.channelName.replace(" ", "-")).removeClass('active-channel');
	}
	$scope.isChannelEnabled = function(n) {
		return $scope.answer.byChannel[n] && $scope.answer.byChannel[n].enabled?true:false;
	}
	$scope.getChannelId = function(n) {
		for (i in $scope.channels) {
			var c = $scope.channels[i];
			if (n == c.name)
				return c.id;
		}
		return null;
	}

	$scope.cmOptions = {
		lineNumbers: true,
		tabMode: "indent",
		onLoad: function(_cm) {
			$scope._cm = _cm;
			$scope.changeMode = function(_m) {
				_cm.setOption('mode', _m);
				$scope.answer.byChannel[$scope.channelName].techTextType = _m;
			}
		}
	};
	var isFirstTT = true;
	$scope.showHideTechText = function() {
		$scope.openTechText=!$scope.openTechText;
		if (isFirstTT) {
			isFirstTT = false;
			$interval(function(){$scope._cm.refresh();},500,1);
		}
	}

	$scope.loadStatus = function() {
		$http.get('/cockpit/v1/answers/status').then(
			//SUCCESS
			function(res) {
				$scope.status = res.data;
			},
			//ERROR
			function(res) {
				console.log(res.data);
				messages.error('ANSWER-LOAD-STATUS-ERROR');
			}
		);
	}

	$scope.loadChannels = function () {
		$http.get('/cockpit/v1/projects/'+project.get().id+'/channels').then(
			//SUCCESS
			function(res) {
				$scope.channels = res.data;
				loading.hide();
			},
			//ERROR
			function(res) {
				console.log(res.data);
				messages.error(JSON.stringify(res.data));
			}
		);
	}

	$scope.saveAnswer = function() {
		$scope.answerError = {};
		var a = $scope.answer;
		var ae = $scope.answerError;
		var error = false;
		if (!a.code) {
			error = true;
			ae.code = 'REQUIRED-FIELD';
		}
		if (!a.title) {
			error = true;
			ae.title = 'REQUIRED-FIELD';
		}
		if (!a.locale) {
			error = true;
			ae.locale = 'REQUIRED-FIELD';
		}
		if (!a.projectId) {
			error = true;
			ae.projectId = 'REQUIRED-FIELD';
		}
		if (!a.type) {
			error = true;
			ae.type = 'REQUIRED-FIELD';
		}
		if (!a.statusId) {
			error = true;
			ae.statusId = 'REQUIRED-FIELD';
		}
		if (!a.type) {
			error = true;
			ae.type = 'REQUIRED-FIELD';
		}
		if (!a.directoryId) {
			error = true;
			ae.directoryId = 'REQUIRED-FIELD';
		}
		

		for (i in a.byChannel) {
			var ac = a.byChannel[i];
			if (ac.answerType == 'HTML') {
				ac.text = ac.html;
			}
			if (!ac.text) {
				error = true;
				ae.text = 'REQUIRED-FIELD';
				ae.html = 'REQUIRED-FIELD';
			}
			for (i in ac.options) {
				var o = ac.options[i];
				o.error = {};

				if (!o.title){
					error = true;
					o.error.title = 'REQUIRED-FIELD';
				}

				if (!o.action) {
					o.textType = 'HTML';
					if (o.textType == 'HTML') {
						o.text = o.html;
					}
					if (!o.text) {
						error = true;
						o.error.text = 'REQUIRED-FIELD';
						o.error.html = 'REQUIRED-FIELD';
					}
				}
				if (o.action) {
					if (!o.text) {
						error = true;
						o.error.text = 'REQUIRED-FIELD';
					}
				}
			}
		}
		
		if (error) {
			messages.error("EMPTY-FIELD");
			return;
		}

		a = angular.copy(a);
		var alist = [];
		for (i in a.byChannel) {
			var ac = a.byChannel[i];
			if (ac.answerType == 'HTML') {
				ac.text = ac.html;
			}
			delete ac.html;
			ac.channelId = $scope.getChannelId(i);
			ac.title = a.title;
			ac.code = a.code;
			ac.locale = a.locale;
			ac.type = a.type;
			ac.transactional = a.transactional == true ? ac.transactional = 1 : ac.transactional = 0;
			ac.statusId = a.statusId;
			ac.type = a.type;
			ac.directoryId = a.directoryId,
			ac.projectId = a.projectId;

			alist.push(ac);
			console.log(alist);
			
			var count = ac.options ? ac.options.length : 0;
			
			for (i in ac.options) {
				var o = ac.options[i];
				delete o.opened;
				
				o.answerId = ac.id;
				o.locale = ac.locale;

				if(o.removed) count--;
			}

			ac.optionsMapping = count > 0;

		}
		
		var uri = '/cockpit/v1/answers';
		var method = 'POST';

		$scope.savingAnswer = true;
		$http({
			"method":method,
			"url":uri,
			"headers":{
				"Content-type":"application/json"
			},
			"data":alist
		}).then(
			//SUCCESS
			function(res) {
				messages.success("ANSWER-CREATED");
				$('#answerModal').modal('hide');
				$scope.loadDirectories($scope.parentDir);
				$scope.savingAnswer = false;
				$scope.answerError = {};
				$scope.answer = {};
			},
			//ERROR
			function(res) {
				console.log(res.data);
				messages.error(res.data.message ? res.data.message : JSON.stringify(res.data));
				$scope.savingAnswer = false;
			}
		);
	}

	$scope.addOption = function() {
		//QUANTIDADE MAXIMA DE BOTOES SUPORTADOS NA ANSWER
		var qtdMax = 12;
		var a = $scope.answer.byChannel[$scope.channelName];
		var qtd = a.options ? a.options.length+1 : 1;
		for (i in a.options) {
			if (a.options[i].removed)
				qtd--;
		}

		if (!a.options)a.options = [];
		if (qtd > qtdMax) {
			messages.warning("ANSWER-MAX-OPTIONS");
			return;
		}
		var opt = {
			"title":'',
			"textType":"HTML",
			"textTypeAction":"WATSON",
			"optionOrder":qtd,
			"error":{}
		};

		a.options.push(opt);
		$scope.openOption(opt);
	};
	$scope.openOption = function(o) {
		if (!o.opened)
			for(i in $scope.answer.byChannel[$scope.channelName].options)$scope.answer.byChannel[$scope.channelName].options[i].opened=false;
		o.opened = !o.opened;
	};

	$scope.loadDirectories = function(dir) {
		if (dir) $scope.parentDir = dir;
		$http.get('/cockpit/v1/answers/' + project.get().id + '/directories/'+(dir ? dir.id + '/load-content' : '')).then(
			//SUCCESS
			function(res) {
				if (dir) {
					dir.children = res.data.directories;
					dir.children.push.apply(res.data.directories, res.data.answers);

					for(i in dir.children) {
						var item = dir.children[i];
						if (item.title) {
							item.isContent = true;
						} else {
							item.isDirectory = true;
						}
					}
				} else {
					$scope.directories = res.data;
					for(i in $scope.directories) {
						var item = $scope.directories[i];
						item.isDirectory = true;
					}
				}
			},
			//ERROR
			function(res) {
				if (dir)dir.openOnce=false;
				console.log(res.data);
				messages.error('ANSWER-LOAD-DIRECTORY-ERROR');
			}
		);
	}

	$scope.selectItem = function(item) {
		if (item.title)
			$scope.selectAnswer(item);
		else
			$scope.selectDirectory(item);
	}
	$scope.selectDirectory = function(d){
		if (!d.parentId)$scope.parentDir=null;
		$scope.directory = angular.copy(d);
		$('#directoryModal').modal("show");
	}
	$scope.selectAnswer = function(a) {
		$http.get("/cockpit/v1/answers/"+project.get().locale+"/"+a.code+"/projects/"+project.get().id).then(
			//SUCCESS
			function (res) {
				var answers = res.data;
				$scope.answer = {
					"title":answers[0].title,
					"code":answers[0].code,
					"statusId":answers[0].statusId+'',
					"type":answers[0].type,
					"directoryId":answers[0].directoryId,
					"projectId":answers[0].projectId,
					"locale":project.get().locale,
					"enabled":answers[0].active,
					"transactional":answers[0].transactional,
					"byChannel":{}
				}
				$('.channel-selector').find("div[id*='channel']").each(function(e){
					console.log("teste" + this);
					this.classList.remove('active-channel');
				});
				for(i in answers) {
					var a = answers[i];
					//a.enabled = true;
					
					if (!a.channelId) {
						$scope.answer.byChannel['ALL'] = a;
						// $('#channel-ALL').addClass('active-channel');
						//$scope.enableContent('ALL');
					} else {
						for (j in $scope.channels) {
							var c = $scope.channels[j];
							if (c.id == a.channelId) {
								$scope.answer.byChannel[c.name] = a;
								
								//$scope.enableContent(c.name);
								break;
							}
						}
												
					}

					if(a.transactional == 1){
						$('#transactional_ck').prop('checked', true);
					}else{
						$('#transactional_ck').prop('checked', false);
					}

					a.answerType = /<[a-z][\s\S]*>/i.test(a.text) ? 'HTML' : 'TEXT';
					for (j in a.options) {
						a.options[j].textType = /<[a-z][\s\S]*>/i.test(a.options[j].text) ? 'HTML' : 'TEXT';
						a.options[j].html = a.options[j].text;
						a.options[j].error = {};
					}
					a.html = a.text;
					answers[i].enabled = answers[i].active;
				}

				for(channel in $scope.answer.byChannel){
					if($scope.answer.byChannel[channel].active === 1)
						$('#channel-'+ channel.replace(" ", "-")).addClass('active-channel');
					else
						$('#channel-'+ channel.replace(" ", "-")).removeClass('active-channel');
				}
				
				$scope.answerError = {};
				$('#answerModal').modal("show");
			},
			//ERROR
			function(res) {
				console.log(res.data);
				messages.error(JSON.stringify(res.data));
			}
		);
	}
	$scope.createDirectory = function(dir) {
		$scope.parentDir = dir;
		$scope.selectDirectory({
			'parentId':dir.id
		});
	}

	 $scope.saveDirectory = function() {
        $scope.directoryError = {};
        var d = $scope.directory;
        var de = $scope.directoryError;
        var error = false;
        
        if (!d.name) {
            error = true;
            de.name = 'REQUIRED-FIELD';
        }

        if (error) return;

		$scope.savingDir = true;
        var method = d.id ? 'PUT' : 'POST';
        var uri = '/cockpit/v1/answers/' + project.get().id + '/directories' + (d.id ? '/'+d.id : '');
        $http({
            "method":method,
            "url":uri,
            "headers":{
                "Content-type":"application/json"
            },
            "data":d
        }).then(
            //SUCCESS
            function(res) {
                messages.success(d.id?"DIRECTORY-ALERT-ALTER-SUCCESS":"DIRECTORY-ALERT-INSERT-SUCCESS",3000);
                $scope.loadDirectories($scope.parentDir);
                $('#directoryModal').modal("hide");
				$scope.savingDir = false;
            },
            //ERROR
            function (res) {
                messages.error(res.data.message ? res.data.message : JSON.stringify(res.data));
                $scope.loadDirectories($scope.parentDir);
                $('#directoryModal').modal("hide");
				$scope.savingDir = false;
            }
        );
    };

	$scope.removeNode = function(item,parent) {
		if (item.name)$scope.removeDirectory(item,parent);
		else  $scope.removeAnswer(item,parent);
	}
	$scope.removeAnswer = function(a,parent) {
		if(!a||!a.code)return;
		confirmModal.show('ANSWER-REMOVE-TITLE', 'ANSWER-REMOVE-TEXT',function(){
			$http.delete("/cockpit/v1/answers/"+project.get().locale+"/"+a.code+"/projects/"+project.get().id).then(
				//SUCCESS
				function(res) {
					messages.success('ANSWER-REMOVE-SUCCESS');
					$scope.loadDirectories(parent);
				},
				//ERROR
					function(res) {
					messages.error(res.data.message?res.data.message:JSON.stringify(res.data));
				}
			);
		});
	}
	$scope.removeDirectory = function(dir,parent){
		if (!dir||!dir.id)return;
		confirmModal.show('DIRECTORY-REMOVE-TITLE', 'DIRECTORY-REMOVE-TEXT',function(){
			$http.delete('/cockpit/v1/answers/' + project.get().id + '/directories/'+dir.id).then(
				//SUCCESS
				function(res) {
					messages.success('DIRECTORY-REMOVE-SUCCESS');
					$scope.loadDirectories(parent);
				},
				//ERROR
					function(res) {
					messages.error(res.data.message?res.data.message:JSON.stringify(res.data));
				}
			);
		});
	}

	$scope.createAnswer = function(item) {
		$scope.channelName = "ALL";
		$scope.parentDir = item; 
		$scope.answer = {
			"title":"",
			"code":"",
			"statusId":"",
			"locale":project.get().locale,
			"type":"",
			"byChannel":{
				"ALL":{
					"answerType":"HTML",
					"techTextType":"javascript"
				}
			},
			"directoryId":item.id,
			"projectId":project.get().id
		};
		$scope.enableContent('ALL');
		$('#answerModal').modal('show');
	}

	$scope.removeOption = function(opt) {
		var a = $scope.answer.byChannel[$scope.channelName];
		opt.removed = true;
		if (a.options.length > opt.optionOrder) {
			for(var i = opt.optionOrder - 1; i < a.options.length - 1; i++) {
				a.options[i].optionOrder--;
			}
		}
	}

	$scope.filter = {};
	$scope.loadAnswerFaqFilter = function() {
		var t = $scope.filter.title;
		if (!t)return;
		t = t.trim();
		if(t.length < 3)return;
		$http.post('/cockpit/v1/answers/select-distinct-faq-name/'+project.get().id, {
			"title":$scope.filter.title
		}).then(
			//SUCCESS
			function(res) {
				$scope.faqsFilter = res.data;
			},
			//ERROR
			function(res) {
				console.log(res.data);
				messages.error('ERROR');
			}
		);
	}
	$scope.loadAnswerCodeFilter = function() {
		var t = $scope.filter.code;
		if (!t)return;
		t = t.trim();
		if(t.length < 3)return;
		$http.post('/cockpit/v1/answers/select-distinct-code/'+project.get().id, {
			"code":$scope.filter.code
		}).then(
			//SUCCESS
			function(res) {
				$scope.codesFilter = res.data;
			},
			//ERROR
			function(res) {
				console.log(res.data);
				messages.error('ERROR');
			}
		);
	}

	$scope.cleanSearch = function() {
		$scope.isSearch = false;
		$scope.filter = {};
		$scope.loadDirectories();
	}
	$scope.isSearch = false;
	$scope.searchFilter = function() {
		var f = $scope.filter;
		if (!f || !f.title && !f.text && !f.code && !f.activeGroup) return;
		console.log('FILTERING',f);
		 $http.post('/cockpit/v1/answers/search/'+project.get().id, f).then(
			//SUCCESS
			function(res) {
				$scope.searchResults = res.data;
				console.log($scope.searchResults);
				$scope.isSearch = true;
				console.log($scope.searchResults[0].activeGroup);
			},
			//ERROR
			function(res) {
				console.log(res.data);
				messages.error('ERROR');
			}
		);
	}

	$scope.exportAnswers = function(){
		window.location.href = uri;
	}

	$scope.activeGroupNode = function(item,parent){
		$http.post('/cockpit/v1/answers/'+project.get().id+'/'+item.code, item).then(
			//SUCCESS
			function(res) {
				item.activeGroup = !item.activeGroup;
			},
			//ERROR
			function(res) {
				messages.error('ERROR');
			}
		);
	}

	$scope.loadDirectories();
	$scope.loadStatus();
	$scope.loadChannels();
});