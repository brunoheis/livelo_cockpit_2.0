/*global require*/
/*
 *    Fill in host and port for Qlik engine
 */
var prefix = ''; //window.location.pathname.substr( 0, window.location.pathname.toLowerCase().lastIndexOf( "/extensions" ) + 1 );

var config = {
	host: 'qlik-everis.everisva.com',
	prefix: '/',
	port: '443',
	isSecure: true
};

// var config = {
// 	host: 'localhost',
// 	prefix: '/',
// 	port: '4848',
// 	isSecure: false
// };

//to avoid errors in workbench: you can remove this when you have added an app
var app;
require.config({
	baseUrl: (config.isSecure ? "https://" : "http://" ) + config.host + (config.port ? ":" + config.port : "" ) + config.prefix + "resources"
});


require( ["js/qlik"], function ( qlik ){

	qlik.setOnError( function ( error ){
		//Error qlik
		window.errorQlikSession = true;
		console.log("Qlik Error setError");

		alert( error.message );
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		qlik.resize();
	});

	//callbacks -- inserted here --
	//open apps -- inserted here --
	
	
	//Version Test DEV
	//var app = qlik.openApp('408429e5-a308-4302-a6b5-da76fc0b4dac', config);

	//Version PROD
	var app = qlik.openApp('3e2cc85e-48eb-4d8b-8c7c-cbeb676b0785', config);

	
	//get objects -- inserted here --
	
	// OBS: IMPORT THE THE FOLLOWING EXTENSIONS IN QLIK SERVER: 
	// 1 - qsSimpleKPI
	// 2 - DateRangePicker
	// 3 - tableCustomAssistentVivi
	//

	  
	// <------------------------ REPORT SATISFAÇÃO --------------------------->
	
	//PART 1
	app.getObject('FILTRO_DATA_SATISFACAO','wdrpPyA');
	app.getObject('GRAFICO_EVOLUCAO_RESPOSTAS','MPTBqVd');
	app.getObject('KPI_MEDIA_REPOSTAS_DIARIAS','cRbgBRE');
	app.getObject('KPI_VOLUME_RESPOSTAS','fTtjpw');
	
	
	//PART 2
	app.getObject('TABELA_TOP12_COMENTARIOS_SATISFACAO','JcCmbUR');
	app.getObject('KPI_DUVIDAS_RESOLVIDAS','pTwUYE');
	app.getObject('GRAFICO_PIZZA_PESQUISA_SATISFACAO_CLIQUES','jbjKxWF');
	
	
	//OUTROS
	app.getObject('GRAFICO_PIZZA_PESQUISA_SATISFACAO','MBFeC');
	app.getObject('FILTRO_PROJETO','LDeNhJ');
	app.getObject('FILTER_SATISFACAO_POSITIVA','ePnmpt');
	
	
	//EXPORTS BUTTON
	app.getObject('EXPORT_EVOLUCAO_RESPOSTAS','RmfyfTc');
	app.getObject('EXPORT_TOP12_COMENTARIOS_SATISFACAO','HQGjPWJ');
	app.getObject('EXPORT_DONUT_PESQUISA_SATISFACAO','aFpTq');
	app.getObject('EXPORT_KPI_DUVIDAS_RESOLVIDAS','xVKmRYt');
	app.getObject('EXPORT_FULL_DATA','aaJJfuf');



	
	//Get value current project
	var indiceProjeto = parseInt(window.parent.qlikProjectActual);
	
	//Clear values filters
	app.unlockAll();
	app.clearAll();

	//Filter project value
	app.field('Projeto').select([indiceProjeto], false, false);
	app.field('Projeto').lock();

	
	if (app) {
		app.getObject('CurrentSelections', 'CurrentSelections').then(function() {
			$(".rain").hide();
			$(".tab-content").show();
			qlik.resize();
		});

		//Loading sucess qlik
		window.carregadoSatisfaction = true;
		console.log("Qlik conect");


	} else {
		$(".rain").hide();
		$(".tab-content").show();

		//Error qlik
		window.errorQlikSatisfaction = true;
		console.log("Qlik Error app");
	}

});