/*global require*/
/*
 *    Fill in host and port for Qlik engine
 */
var prefix = ''; //window.location.pathname.substr( 0, window.location.pathname.toLowerCase().lastIndexOf( "/extensions" ) + 1 );

var config = {
	host: 'qlik-everis.everisva.com',
	prefix: '/',
	port: '443',
	isSecure: true
};


//to avoid errors in workbench: you can remove this when you have added an app
var app;
require.config({
	baseUrl: (config.isSecure ? "https://" : "http://" ) + config.host + (config.port ? ":" + config.port : "" ) + config.prefix + "resources"
});


require( ["js/qlik"], function ( qlik ){

	qlik.setOnError( function ( error ){
		//Error qlik
		window.errorQlikSession = true;
		console.log("Qlik Error setError");

		alert( error.message );
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		qlik.resize();
	});

	//callbacks -- inserted here --
	//open apps -- inserted here --

	//Version Test DEV
	//var app = qlik.openApp('408429e5-a308-4302-a6b5-da76fc0b4dac', config);


	//Version PROD
	var app = qlik.openApp('3e2cc85e-48eb-4d8b-8c7c-cbeb676b0785', config);
	
	


	//<-------------------------- PARMS AREA --------------------------->

	//Get value current project
	var indiceProjeto = parseInt(window.parent.qlikProjectActual);
	console.log('indiceProjeto',indiceProjeto);

	//Clear values filters
	app.unlockAll();
	app.clearAll();

	//Filter project value
	app.field('Projeto').select([indiceProjeto], false, false);
	app.field('Projeto').lock();



	// OBS: IMPORT THE THE FOLLOWING EXTENSIONS IN QLIK SERVER: 
	// 1 - qsSimpleKPI
	// 2 - DateRangePicker
	// 3 - tableCustomAssistentVivi
	//
	// <-------------------------- REPORT SESSION --------------------------->
	
	//PART 1
	app.getObject('GRAFICO_EVOLUCAO_SESSOES_CANAIS','KRvamE');
	app.getObject('KPI_PROJECAO_SESSOES_MES','AJXqA');
	app.getObject('KPI_MEDIA_SESSOES_DIARIAS','QjTAKE');
	app.getObject('KPI_MEDIA_PERGUNTAS_SESSAO','QyzQHW');
	app.getObject('FILTRO_DATA_SESSION','AMyus');
	app.getObject('FILTRO_CANAL','sVUWytq');

	
	//PART 2
	app.getObject('KPI_DADOS_GERAIS_SESSOES','JCnPzX');
	app.getObject('BAR_PORCENTAGEM_SESSOES_ATENDIDAS','gREmeeP');
	app.getObject('KPI_DADOS_GERAIS_INFORMACIONAL','apLpr');
	app.getObject('BAR_PORCENTAGEM_SESSOES_INFORMACIONAIS','YujXJ');
	app.getObject('KPI_DADOS_GERAIS_ENCONTRADOS','QjApJpx');
	app.getObject('KPI_DADOS_GERAIS_NAO_ENCONTRADOS','HbWCbZX');
	app.getObject('BAR_PORCENTAGEM_SESSOES_ENCONTRADAS','mACHfP');
	

	//PART 3
	app.getObject('TABELA_TOP10_FAQ','TwQYbh');
	app.getObject('GRAFICO_RANKING_TOP10_FAQ','XcPSbu');
	app.getObject('TABELA_TOP10_USER_INPUT','RCh');
	
	
	app.getObject('FILTRO_PROJETO','LDeNhJ');
	app.getObject('FILTRO_CANAL_PRINCIPAL','ZxvAFC');


	//EXPORTS BUTTON
	app.getObject('EXPORT_DADOS_GERAIS','JxPZPY');
	app.getObject('EXPORT_KPI_DADOS_ECONTRADOS','pdZx');
	app.getObject('EXPORT_RANKING_FAQ','nsF');
	app.getObject('EXPORT_TABLE_USER_INPUT','vVPxPA');
	app.getObject('EXPORT_TABLE_FAQ','APxGy');

	//app.getObject('btnFilterClear','BPfTy');
		// app.createList({
	// "qDef": {
	// 	"qFieldDefs": [
	// 		"[Data Sessão]"
	// 	]
	// },
	// "qInitialDataFetch": [{
	// 		qTop : 0,
	// 		qLeft : 0,
	// 		qHeight : 2000,
	// 		qWidth : 1
	// 	}]
	// }, function(reply) {
	// 	console.log(reply);
	// });
	
	// var fieldDa = app.field('Quantidade data sessão');
  	// fieldDa.getData().rows[0];
	// console.log(fieldDa);
	// console.log(fieldDa.rowCount);

	// app.createGenericObject( {   
    //  countDateSession: { qValueExpression: "=count(distinct [Data Sessão])" }   
	// }, function ( reply ) {  
	// 	qtDateSession = reply.countDateSession;	
	// 	console.log("reply",reply);
	// });


	// var fieldDa = app.field('PARM.contagemDataSessao');
  	// fieldDa.getData();
	// console.log(fieldDa);
	// console.log(fieldDa.rows);

	// for (var row in fieldDa.rows) {
  	// 	console.log("key " + key + " has value " + rows[key]);
	// }

	// daysSelect = 7;
	// for (cont=qtDateSession-1;cont>=(qtDateSession-daysSelect);cont--){
	// 	if(cont>0){
	// 		var ind = parseInt(cont);
	// 		app.field('[Data Sessão]').select([ind], true, true);
	// 		console.log("cont",cont);
	// 	}
	// }


	if (app) {
		app.getObject('CurrentSelections', 'CurrentSelections').then(function() {
			$(".rain").hide();
			$(".tab-content").show();
			qlik.resize();
		});

		//Loading sucess qlik
		window.carregadoSession = true;
		console.log("Qlik conect");

	} else {
		$(".rain").hide();
		$(".tab-content").show();

		//Error qlik
		window.errorQlikSession = true;
		console.log("Qlik Error app");
	}


});