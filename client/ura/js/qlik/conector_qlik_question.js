/*global require*/
/*
 *    Fill in host and port for Qlik engine
 */
var prefix = ''; //window.location.pathname.substr( 0, window.location.pathname.toLowerCase().lastIndexOf( "/extensions" ) + 1 );

var config = {
	host: 'qlik-everis.everisva.com',
	prefix: '/',
	port: '443',
	isSecure: true
};


//to avoid errors in workbench: you can remove this when you have added an app
var app;
require.config({
	baseUrl: (config.isSecure ? "https://" : "http://" ) + config.host + (config.port ? ":" + config.port : "" ) + config.prefix + "resources"
});



require( ["js/qlik"], function ( qlik ){

	qlik.setOnError( function ( error ){
		//Error qlik
		window.errorQlikQuestion = true;
		console.log("Qlik Error setError");

		alert( error.message );
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		qlik.resize();
	});

	//callbacks -- inserted here --
	//open apps -- inserted here --

	//Version Test DEV
	//var app = qlik.openApp('408429e5-a308-4302-a6b5-da76fc0b4dac', config);


	//Version PROD
	var app = qlik.openApp('3e2cc85e-48eb-4d8b-8c7c-cbeb676b0785', config);
	
	
	//get objects -- inserted here --
	
	// OBS: IMPORT THE THE FOLLOWING EXTENSIONS IN QLIK SERVER: 
	// 1 - qsSimpleKPI
	// 2 - DateRangePicker
	// 3 - tableCustomAssistentVivi
	//
	
	 
	// <-------------------------- REPORT QUESTIONS --------------------------->
	
	//PART 1
	app.getObject('GRAFICO_EVOLUCAO_PERGUNTAS','phpML');
	app.getObject('KPI_VOLUME_PERGUNTAS','fysV');
	app.getObject('KPI_MEDIA_PERGUNTAS_DIARIAS','pJDXXQq');

	
	//PART 2
	app.getObject('KPI_STATUS_PERGUNTAS_TOTAL','SWDtJy');
	app.getObject('BAR_PORCENTAGEM_STATUS_PERGUNTAS','HQpm');
	app.getObject('KPI_STATUS_PERGUNTA_ENCONTRADAS','JmbJR');
	app.getObject('KPI_STATUS_PERGUNTA_NAO_ENCONTRADAS','btajWSG');
	app.getObject('KPI_STATUS_PERGUNTA_CHIT_CHAT','VCykdUx');
	app.getObject('BAR_PORCENTAGEM_STATUS_PERGUNTAS_ENCONTRADAS','rhszpm');
	app.getObject('KPI_MEDIA_PERGUNTAS_DIARIAS_POR_USUARIO','mkcpV');

	
	
	//PART 3
	app.getObject('TABELA_TOP5_PERGUNTAS','zckXU');
	app.getObject('TABELA_TOP5_COMENTARIOS_SATISFACAO','Abnsnr');
	app.getObject('GRAFICO_PIZZA_PESQUISA_SATISFACAO','MBFeC');
	app.getObject('GRAFICO_PIZZA_PESQUISA_QUANTIDADE_CANAL','kXbrgc');
	

	//PART 4
	app.getObject('FILTRO_FAQ','JYEkZa');
	app.getObject('FILTRO_PALAVRA_USER_INPUT','fjyQPX');
	app.getObject('FILTRO_PERGUNTAS','nXpYmNF');	
	
	//OUTROS
	app.getObject('FILTRO_DATA_SESSION','AMyus');
	app.getObject('FILTRO_CANAL','sVUWytq');
	
	app.getObject('FILTRO_PROJETO','LDeNhJ');
	app.getObject('FILTRO_CANAL_PRINCIPAL','ZxvAFC');
	//app.getObject('FILTER_PERGUNTAS_ENCONTRADOS','mTmCFG');


	//EXPORTS BUTTON
	app.getObject('EXPORT_EVOLUCAO_PERGUNTAS','PpRTrg');
	app.getObject('EXPORT_KPI_STATUS_PERGUNTA','RmYHYQF');
	app.getObject('EXPORT_TABELA_PERGUNTAS','ELgPj');
	app.getObject('EXPORT_TABELA_COMENTARIOS_SATISFACAO','PGYsnd');
	app.getObject('EXPORT_DONUT_PESQUISA_QUANTIDADE_CANAL','AMBAh');
	app.getObject('EXPORT_DONUT_PESQUISA_SATISFACAO','rEYqvq');
	
	//Get value current project
	var indiceProjeto = parseInt(window.parent.qlikProjectActual);
	
	//Clear values filters
	app.unlockAll();
	app.clearAll();

	//Filter project value
	app.field('Projeto').select([indiceProjeto], false, false);
	app.field('Projeto').lock();

	if (app) {
		app.getObject('CurrentSelections', 'CurrentSelections').then(function() {
			$(".rain").hide();
			$(".tab-content").show();
			qlik.resize();
		});
		
		//Loading sucess qlik
		window.carregadoQuestion = true;
		console.log("Qlik conect");
		
	} else {
		$(".rain").hide();
		$(".tab-content").show();

		//Error qlik
		window.errorQlikQuestion = true;
		console.log("Qlik Error app");
	}

});