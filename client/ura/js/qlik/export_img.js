
//Creating dynamic link that automatically click
function downloadURI(uri, name) {
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
}

//Your modified code.
function printToFile(div, nameReport) {

    // document.getElementById("EXPORT_KPI_DADOS_ECONTRADOS").style.visibility = "hidden";    
    
    html2canvas(div, {
        onrendered: function (canvas) {
            var myImage = canvas.toDataURL("image/png");
            //create your own dialog with warning before saving file
            //beforeDownloadReadMessage();
            //Then download file
            downloadURI("data:" + myImage, nameReport+".png");
            // document.getElementById("EXPORT_KPI_DADOS_ECONTRADOS").style.visibility = "visible";
        }
    });
}