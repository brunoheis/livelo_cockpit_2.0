$(function () {

    $('#cmd').click(function () {
        var doc = new jsPDF('l', 'pt', 'a4');
        doc.addHTML($('#content'), 30,30, {
        'pagesplit':true,
        'background': '#e6eeef',
    }, function() {
        doc.save('export-sessão.pdf');
        });
    });

});