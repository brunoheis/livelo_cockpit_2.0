var app = angular.module("cockpitApp");

app.controller('projectsCtrl',function($scope, $http, $state,$interval,leftBar,confirmModal,messages,project,loading) {
	leftBar.hide();
	$scope.project = {};
	
	$scope.loadProjects = function() {
		$http.get('/cockpit/v1/projects/').then(function(res){
			$scope.projects = res.data;

			//Add index number in projets to use in the Qlik
			for (i in $scope.projects) {
				$scope.projects[i].order = i;
			}	

			resizeProjects($scope.projects.length + 1);
			loading.hide();
		});
	};


	$scope.selectProjectUser = function (userid) {
		$http.get('/cockpit/v1/projects/select-by-userid/'+userid).then(function(res){
			$scope.projects = res.data;
			resizeProjects($scope.projects.length + 1);
		})
	};
		
	$scope.projectDashboard = function(p) {
		
	
		project.set(p);

		//Set index of project
		window.qlikProjectActual = p.order;
		

		loading.show();

		$state.go('answer');
	}

	$scope.selectProject = function(p){
		$scope.project = angular.copy(p);

		$('#projectModal').modal({"show":true});
	}

	$scope.removeProject = function(pid){
		confirmModal.show('PROJECT-REMOVE-TITLE', 'PROJECT-REMOVE-TEXT',function(){
			$http.delete('/cockpit/v1/projects/'+pid).then(
				//SUCCESS
				function(res) {
					messages.success('PROJECT-REMOVE-SUCCESS');
					$scope.loadProjects();
				},
				//ERROR
					function(res) {
					messages.error(res.data.message?res.data.message:JSON.stringify(res.data));
				}
			);
		});
	}


	$scope.saveProject = function() {
		$scope.projectError = {};
		var p = $scope.project;
		var pe = $scope.projectError;
		var error = false;
		if (!p.name) {
			error = true;
			pe.name = 'REQUIRED-FIELD';
		}
		if (!p.workspaceId) {
			error = true;
			pe.workspaceId = 'REQUIRED-FIELD';
		}
		if (!p.workspaceUsername) {
			error = true;
			pe.workspaceUsername = 'REQUIRED-FIELD';
		}
		if (!p.workspacePassword) {
			error = true;
			pe.workspacePassword = 'REQUIRED-FIELD';
		}
        if (p.locale == 'default') {
            error = true;
            pe.locale = 'REQUIRED-FIELD';
        }
		if (error) return;

		$scope.hideButton = true;

		delete p.image;
		delete p.contentType;

		if (p.imageTemp && p.contentTypeTemp) {
			p.image = p.imageTemp;
			p.contentType = p.contentTypeTemp;
			delete p.imageTemp;
			delete p.contentTypeTemp;
		}

		var method = p.id ? 'PUT' : 'POST';
		var uri = '/cockpit/v1/projects' + (p.id ? '/'+p.id : '');
		$http({
				"method":method,
				"url":uri,
				"headers":{
				"Content-type":"application/json"
			},
			"data":p
		}).then(
			//SUCCESS
			function(res) {
				$scope.loadProjects();
				messages.success(p.id?"PROJECT-UPDATED":"PROJECT-CREATED");
				$('#projectModal').modal("hide");
				$scope.hideButton = false;
				$("#imageSelector").val("");
				$scope.project.image = "";
				$scope.nameImage = "";
			},
			//ERROR
			function (res) {
				console.log(res.data);
				messages.error(res.data.message ? res.data.message : JSON.stringify(res.data));
				$scope.loadProjects();
				$('#projectModal').modal("hide");
				$scope.hideButton = false;
				$("#imageSelector").val("");
				$scope.project.image = "";
				$scope.nameImage = "";
			}
		);
	};

	$scope.getImage = function(e) {
		if (!e || !e.files || !e.files.length || !e.files[0].size)return;
		if (e.files[0].size <= 102400) {
			var reader = new FileReader();
			reader.onload = function(e){
				var project = reader.result;
				var size = project.indexOf(',')+1;
				$scope.project.imageTemp = project.substr(size);
				$scope.project.contentTypeTemp = project.substr(0,size);
				$scope.$apply();
			};
			reader.readAsDataURL(e.files[0]);
			$scope.nameImage = e.files[0].name;
			$scope.$apply();
		} else {
			$("#imageSelector").val("");
			$scope.project.image = "";
			$scope.project.contentType = "";
			$scope.nameImage = "";
			messages.warning("PROJECT-IMAGE-SIZE");
			return;
		}

	};

	$scope.loadProjects();

	$scope.openImageSelector = function(e) {
		e.preventDefault();
		e.stopPropagation();
		document.querySelector('#imageSelector').click();
	}
});


function resizeProjects(n) {
	var prSize = 272;
	var pl = $('#projectsList');
	var sw = window.innerWidth-23;
	var num = typeof(n)=='number'?n:pl.children().length;
	var wid = num * prSize;
	if (wid > sw)
		wid = parseInt(sw / prSize) * prSize;
	pl.css('width', wid+'px');
}
$(window).resize(resizeProjects);
