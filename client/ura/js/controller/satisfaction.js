var app = angular.module("cockpitApp");


app.controller('satisfactionCtrl',function($scope, $http, $location, $interval, leftBar, confirmModal, messages, loading) {
    
    leftBar.show();
    loading.hide();
    
    // SCRIPT LOADING REPORT 
    $scope.qlikLoading = function (){

        document.getElementById("iframeSatisfaction").contentWindow.carregadoSatisfaction = false;
        document.getElementById("iframeSatisfaction").contentWindow.errorQlikSatisfaction = false;
            
        function afterQlikLoad() { 
            console.log('FLAGENTROU = ENTROU');
            $('.imageLoading').css('display','none');
            $('.reportSatisfaction').css('visibility','visible').attr('id','animationReport');
        }

        function checkQlik() {
            // console.log(index);
            console.log('FLAG = ',document.getElementById("iframeSatisfaction").contentWindow.carregadoSatisfaction);

            if (document.getElementById("iframeSatisfaction").contentWindow.carregadoSatisfaction || document.getElementById("iframeSatisfaction").contentWindow.errorQlikSatisfaction){
                $interval(afterQlikLoad, 3000, 1);
            } else {
                $interval(checkQlik, 2000, 1);
            }
            
        };
        $interval(checkQlik, 2000, 1);
    }

    $scope.qlikLoading();

});








