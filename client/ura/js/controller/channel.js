var app = angular.module("cockpitApp");

app.controller('channelsCtrl',function($scope, $http, $location, leftBar, confirmModal, messages, project, loading) {
    
    leftBar.show('channels');

    $scope.projects = [];
    $scope.channelClassifications = [];
    $scope.channels = [];
    $scope.channel = {};
    $scope.channelError = {};
	
    var baseUri = function () {
        return "/cockpit/v1/projects/" + project.get().id + "/channels/";
    }


    //<----- Channel ------>
   
    $scope.loadChannels = function() {
        $http.get(baseUri()).then(function(res){
            $scope.channels = res.data;
            loading.hide();
        });
    };

    $scope.selectChannel = function(c){
        $scope.channelError = {};
        c.classificationId = c.classificationId?c.classificationId+'':'';
        $scope.channel = angular.copy(c);
        $('#channelModal').modal({"show":true});
        $scope.labelModalChannel = (c.id?"CHANNEL-LABEL-ALTER-CHANNEL":"CHANNEL-LABEL-NEW-CHANNEL");
    };

    $scope.removeChannel = function(cid) {
        confirmModal.show('CHANNEL-LABEL-REMOVE','CHANNEL-LABEL-DESCRIPTION-REMOVE', function() {
            $http.delete(baseUri()+cid).
            then(
                function(){
                    messages.success("CHANNELS-ALERT-DELETE-SUCCESS",1000);
                    $scope.loadChannels();
                },
                function(){
                    messages.success("CHANNELS-ALERT-DELETE-FAIL",1000);
                    $scope.loadChannels();
                }
            );
        });
    };

    $scope.saveChannel = function() {
        $scope.channelError = {};
        var c = $scope.channel;
        var ce = $scope.channelError;
        var error = false;
        
        if (!c.name) {
            error = true;
            ce.name = 'campo obrigatório';
        }
        if (!c.classificationId) {
            error = true;
            ce.classificationId = 'campo obrigatório';
        }

        c.projectId = project.get().id;

        if (error) return;

        $scope.savingChannel = true;
        var method = c.id ? 'PUT' : 'POST';
        var uri = baseUri() + (c.id ? c.id : '');
        $http({
            "method":method,
            "url":uri,
            "headers":{
                "Content-type":"application/json"
            },
            "data":c
        }).then(
            //SUCCESS
            function(res) {
                messages.success(c.id?"CHANNELS-ALERT-ALTER-SUCCESS":"CHANNELS-ALERT-INSERT-SUCCESS",3000);
                $scope.loadChannels();
                $('#channelModal').modal("hide");
                $scope.savingChannel = false;
            },
            //ERROR
            function (res) {
                messages.error(res.data.message ? res.data.message : JSON.stringify(res.data));
                $scope.loadChannels();
                $('#channelModal').modal("hide");
                $scope.savingChannel = false;
            }
        );
    };



    //<----- Channel Classification ------>

    $scope.loadChannelClassification = function() {
        $http.get('/cockpit/v1/channelClassifications').then(function(res){
            $scope.channelClassifications = res.data;
        });
    };


    $scope.loadChannels();
    $scope.loadChannelClassification();


});
