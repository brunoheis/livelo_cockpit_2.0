var app = angular.module("cockpitApp");
//var loadingQlik = require('../qlik/qlik_loading.js');

app.controller('questionCtrl',function($scope, $http, $location, $interval, leftBar, confirmModal, messages, loading) {
    
    leftBar.show();
    loading.hide();

    // SCRIPT LOADING REPORT 
    $scope.qlikLoading = function (){

            document.getElementById("iframeQuestion").contentWindow.carregadoQuestion = false;
            document.getElementById("iframeQuestion").contentWindow.errorQlikQuestion = false;
            
            function afterQlikLoad() { 
                console.log('FLAGENTROU = ENTROU');
                $('.imageLoading').css('display','none');
                $('.reportQuestion').css('visibility','visible').attr('id','animationReport');
            }

            
            function checkQlik() {
                console.log('FLAG = ',document.getElementById("iframeQuestion").contentWindow.carregadoQuestion);

                if (document.getElementById("iframeQuestion").contentWindow.carregadoQuestion ||  document.getElementById("iframeQuestion").contentWindow.errorQlikQuestion){
                    $interval(afterQlikLoad, 3000, 1);
                } else {
                    $interval(checkQlik, 2000, 1);
                }
                
            };
            $interval(checkQlik, 2000, 1);
    }

    $scope.qlikLoading();



});








